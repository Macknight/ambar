Desafio Ambar - Previsão do tempo

## Instruções:

- Clone o projeto:
> git clone git@bitbucket.org:Macknight/ambar.git
- Mude para a pasta em que foi clonado o projeto:
> cd ./ambar
- Utilize o npm para rodar estanciar um servidor:
> npm start
- Acesse o browser no seguinte endereço:
> localhost:3000

