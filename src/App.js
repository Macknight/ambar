import React from 'react';
import Title from './components/Title';
import WeatherButtons from './components/WeatherButtons';
import WeatherDisplay from './components/WeatherDisplay';
import MaxMin from './components/MaxMin'

import store from './store';

import axios from 'axios';
import { Provider } from 'react-redux';


const MK_API_KEY = 'fda64e2710ea6f99085927a945e4a59e';

export default class App extends React.Component {
  state = {
    minTemp: undefined,
    maxTemp: undefined,
    currentTemp: undefined,
    cityName: undefined,
    error: undefined,
    iconURL: undefined,
  }
  getWeatherInfo = async (e) => {
    // const cityName = e.target.elements.name.cityName.value;
    const cityName = e.target.name
    // const request = `https://cors-anywhere.herokuapp.com/https://samples.openweathermap.org/data/2.5/weather?q=${cityName},br&appid=${MK_API_KEY}`;
    const request = `http://api.openweathermap.org/data/2.5/weather?q=${cityName},br&appId=${MK_API_KEY}&units=metric`;
    axios.get(
      request
    ).then(res => {
      const fullURL = `http://openweathermap.org/img/wn/10n@2x.png`

      this.setState({
        maxTemp: res.data.main.temp_max,
        cityName: res.data.name,
        minTemp: res.data.main.temp_min,
        currentTemp: res.data.main.temp,
        iconURL: fullURL,

      })
    });
  }

  clearState = async () => {
    this.setState({
      minTemp: undefined,
      maxTemp: undefined,
      currentTemp: undefined,
      cityName: undefined,
      error: undefined,
      iconURL: undefined,
      showMaxMin: true,
    });
  }

  gotoMaxMin = async () => {
    window.location.href = 'http://localhost:3000/MaxMin';
  }

  render() {
    return (
      <div >
        <Provider store={store}>
          <div className="wrapper">
            <div className="main">
              <div className="container">
                <div className="row">

                  <Title />

                  <WeatherButtons getWeatherInfo={this.getWeatherInfo} clearState={this.gotoMaxMin} />
                  <hr />
                  {this.state.cityName && <div>
                    <WeatherDisplay
                      maxTemp={this.state.maxTemp}
                      cityName={this.state.cityName}
                      minTemp={this.state.minTemp}
                      currentTemp={this.state.currentTemp}
                      iconURL={this.state.iconURL}
                    />

                  </div>}
                  {this.state.showMaxMin && <MaxMin maxTemp={this.state.maxTemp} />}
                </div>
              </div>
            </div>
          </div>
        </Provider>
      </div>
    );
  }
}

