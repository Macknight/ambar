import React, { Component } from 'react';

class Title extends Component {
	state = {}
	render() {
		return (
			<div className="container ">
				<div className="jumbotron header-nav">
					<div className="row margin-top-5">
						<div className="col">
							<h1> <img src="https://www.ambar.tech/wp-content/uploads/2019/07/logo-ambar-color-2x.png"
								width="142" height="68"
								alt="Ambar Logo"
								className="fusion-standard-logo"></img> </h1>
						</div>
						<div className="col">
							<h2 className="text-secondary">Previsão do tempo</h2>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Title;