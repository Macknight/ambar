import React, { Component } from 'react';

class Main extends Component {
	state = {}
	render() {
		return (
			<div >
				<div className="wrapper">
					<div className="main">
						<div className="container">
							<div className="row">
								<Title />
								<WeatherButtons getWeatherInfo={this.getWeatherInfo} clearState={this.clearState} />
								<hr />
								{this.state.cityName && <div>
									<WeatherDisplay
										maxTemp={this.state.maxTemp}
										cityName={this.state.cityName}
										minTemp={this.state.minTemp}
										currentTemp={this.state.currentTemp}
										iconURL={this.state.iconURL}
									/>

								</div>}
								{this.state.showMaxMin && <MaxMin maxTemp={this.state.maxTemp} />}
							</div>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default Main;