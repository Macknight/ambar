import React, { Component } from 'react';
import { connect } from 'react-redux';

class WeatherDisplay extends Component {

	constructor(props) {
		super(props);
		this.state = {}
	}

	toggleMaxMin = (currentTemp, cityName) => {
		return {
			type: 'TOOGLE_MAX_MIN',
			currentTemp,
			cityName,

		};
	}
	render() {
		console.log('weatherDisplay props', this.props);
		if (this.props.cityName) {
			this.props.dispatch(this.toggleMaxMin(this.props.currentTemp, this.props.cityName))
		}
		return (
			<div className="container">
				{this.props.cityName && <h3>{this.props.cityName}</h3>}
				<div className="jumbotron info-holder">
					{this.props.maxTemp && <p>Temperatura máxima: <span className="info">{this.props.maxTemp}</span> </p>}
					{this.props.minTemp && <p>Temperatura mínima: <span className="info">{this.props.minTemp}</span> </p>}
					{this.props.currentTemp && <p>Temperatura atual: <span className="info">{this.props.currentTemp}</span> </p>}
					{this.props.iconURL && <p>Clima:
						<img src={this.props.iconURL}
							width="50" height="50" /></p>
					}
				</div>
			</div>
		);
	}
}
export default connect()(WeatherDisplay);