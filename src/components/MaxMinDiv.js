import React, { Component } from 'react';
import { connect } from 'react-redux';

// const MaxMinDiv = ({ data }) => (

// 	<div>
// 		<p>{data.highestTemp}</p>
// 		<button className="btn btn-danger" onClick={this.props.clearState} > Maximas e Mínimas</button>
// 	</div>
// );
class MaxMinDiv extends Component {
	constructor(props) {
		super(props);
		this.state = {}

	}
	render() {
		return (
			<div className='container'>
				<div className=' jumbotron maxHolder'>
					<p> Temperatura máxima : <span className='maxNumber'>{this.props.data.highestTemp}</span></p>
					{this.props.data.highestTempCityName && <p>{this.props.data.highestTempCityName}</p>}
				</div>
				<div className='jumbotron minHolder'>
					<p> Temperatura mínima : <span className='minNumber'>{this.props.data.lowestTemp}</span></p>
					{this.props.data.highestTempCityName && <p>{this.props.data.lowestTempCityName}</p>}
				</div>
			</div>
		);
	}
}

export default connect(state => ({ data: state }))(MaxMinDiv);