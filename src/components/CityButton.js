import React, { Component } from 'react';

class CityButton extends Component {
	render() {
		return (
			<button className="btn btn-primary city-btn" onClick={this.props.getWeatherInfo}
				name={this.props.cityName}>{this.props.cityName}</button>
		);
	}
}

export default CityButton;