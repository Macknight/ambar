import React, { Component } from 'react';
import Title from './Title'
import store from '../store';
import MaxMinDiv from './MaxMinDiv';
import { Provider } from 'react-redux';
class MaxMin extends Component {
	render() {
		return (
			<div className="container">
				<Title />
				<Provider store={store}>
					<MaxMinDiv />
				</Provider>

			</div>

		);
	}
}


export default MaxMin;