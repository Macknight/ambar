import React, { Component } from 'react';
import CityButton from './CityButton';

class WeatherButtons extends Component {
	state = {}
	render() {
		return (
			<div className="container">
				<CityButton getWeatherInfo={this.props.getWeatherInfo} cityName='São Carlos' />
				<CityButton getWeatherInfo={this.props.getWeatherInfo} cityName='Araraquara' />
				<CityButton getWeatherInfo={this.props.getWeatherInfo} cityName='Rio Claro' />
				<CityButton getWeatherInfo={this.props.getWeatherInfo} cityName='Pindamonhangaba' />
				<button className="btn btn-danger" onClick={this.props.clearState} > Maximas e Mínimas</button>
			</div >
		);
	}
}

export default WeatherButtons;