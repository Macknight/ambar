import { createStore } from 'redux';

const INITIAL_STATE = {
	highestTemp: -99,
	highestTempCityName: undefined,
	lowestTemp: 99,
	lowestTempCityName: undefined,
};

function reducer(state = INITIAL_STATE, action) {

	const lastState = localStorage.getItem("state");
	if (state === INITIAL_STATE) {
		if (lastState != null) {
			try {
				let obj = JSON.parse(lastState);
				state = obj;
			} catch (e) {
				console.log('deu pau');
			}
		}
	}

	// checa se valores precisam ser atualizados e atualiza
	if (action.type === 'TOOGLE_MAX_MIN') {
		if (action.currentTemp > state.highestTemp) {
			localStorage.setItem("state", state);
			state = {
				...state,
				highestTemp: action.currentTemp,
				highestTempCityName: action.cityName
			}
		}
		if (action.currentTemp < state.lowestTemp) {
			localStorage.setItem("state", state);
			state = {
				...state,
				lowestTemp: action.currentTemp,
				lowestTempCityName: action.cityName
			}
		}
		localStorage.setItem("state", JSON.stringify(state));
		return state;
	}
	return state;
}
const store = createStore(reducer);

export default store;